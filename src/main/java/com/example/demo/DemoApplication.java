package com.example.demo;

import com.example.demo.model.Item;
import com.example.demo.model.User;
import com.example.demo.repository.ItemRepository;
import com.example.demo.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.*;

@SpringBootApplication
public class DemoApplication {
    private static final Logger log =  LoggerFactory.getLogger(DemoApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);


    }

    @Bean
    public CommandLineRunner demo(UserRepository userRepository, ItemRepository itemRepository) {
        return (args) -> {
            // Add Users!!
            userRepository.save(new User("Δημήτριος", "Ιωάννου","2321054819","dimioanou@gmail.com"));
            userRepository.save(new User("Αθανάσιος", "Πέτρου","2310846315","athanpetrou@gmail.com"));
            userRepository.save(new User("Μαρία", "Ισπανού","2108761353","marispanou@gmail.com"));
            userRepository.save(new User("Κωνσταντίνα", "Αποστόλου","2103794319","konapost@gmail.com"));
            userRepository.save(new User("Γεώργιος", "Βασιλείου","2310934765","georbasil@gmail.com"));
            // Add Items!!
            itemRepository.save(new Item("Samsung Galaxy S20 (128GB)","Somtehing about the product!!","346734674"));
            itemRepository.save(new Item("Lg Smart TV 60'","Somtehing about the product!!","6754785672"));
            itemRepository.save(new Item("Apple iPad 2020 10.2 (32GB) 7th Generation","Somtehing about the product!!","96634525667"));
            itemRepository.save(new Item("Laptop Dell Inspiron 15.6","Somtehing about the product!!","2495693477329"));
            itemRepository.save(new Item("HP Laser 107a","Somtehing about the product!!","527894612367"));
            // 
            User user = new User("Βασίλης","Αναστασίου","2310896216","basilanast@gmail.com");
            List<Item> items = List.of(itemRepository.findByBarcode("96634525667"),itemRepository.findByBarcode("6754785672"));
            user.setItems(items);
            userRepository.save(user);
        };
    }
}
