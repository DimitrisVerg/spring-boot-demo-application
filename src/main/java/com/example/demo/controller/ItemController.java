package com.example.demo.controller;

import com.example.demo.model.Item;
import com.example.demo.repository.ItemRepository;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Optional;

@RestController
public class ItemController {

    private final ItemRepository itemRepository;

    public ItemController(ItemRepository itemRepository) {
        this.itemRepository = itemRepository;
    }

    @GetMapping("/items")
    List<Item> all(){
        return itemRepository.findAll();
    }

    @GetMapping(value = "/item-barcode/{barcode}")
    public Item getItemByIBarcode(@PathVariable String barcode) {
        return itemRepository.findByBarcode(barcode);
    }

    @GetMapping(value = "/item/{id}")
    public Optional<Item> getItemById(@PathVariable Long id) {
        return itemRepository.findById(id);
    }

    @PostMapping("/items")
    Item newItem(@RequestBody Item newItem){
        return itemRepository.save(newItem);
    }

    @DeleteMapping("/item/{id}")
    void deleteItem(@PathVariable Long id) {
        itemRepository.deleteById(id);
    }

    @PutMapping("/item/{id}")
    Item updateItem(@RequestBody Item newItem, @PathVariable Long id) {

        return itemRepository.findById(id)
                .map(user -> {
                    user.setName(newItem.getName());
                    user.setDescription(newItem.getDescription());
                    user.setBarcode(newItem.getBarcode());
                    return itemRepository.save(user);
                })
                .orElseGet(() -> {
                    newItem.setId(id);
                    return itemRepository.save(newItem);
                });
    }
}
